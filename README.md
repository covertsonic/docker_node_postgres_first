# docker_node_postgres_first

My script (for funzies) to help the team with migration from PHP and Python to Node.

## Components

- Docker - Creates a consistent deployment experience (no issues with server configuration)
- Node.JS - The underlying technology.
- Express - Node.JS framework
- React - Front-end templating engine

## Running the App

### Without Docker:

> npm run start

### With Docker:

First, build the docker image. The image steps are defined inside Dockerfile.

> docker build -t docker_node_postgres_first .

Then run the image:
On Windows:

> docker run -it -p 3000:3000 -v \${pwd}:/app docker_node_postgres_first

On Linux (or cross platform PowerShell):

> docker run -it -p 3000:3000 -v \$(pwd):/app docker_node_postgres_first

## Other fun facts I learned

- To use nodemon outside of a node script, you need to install it with the global flag: npm install --save -g nodemon
